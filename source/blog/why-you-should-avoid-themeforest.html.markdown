---
title: Why you should avoid ThemeForest
date: 2016-09-04 08:51 UTC
description: ThemeForest promotes monolithic 'multi-purpose' themes that ultimately result in long loading times and sluggish performance. Your business should go with a custom designed WordPress theme instead.
---

ThemeForest is the overwhelming market leader when it comes to premium WordPress themes and has become the first point of call for many businesses looking to start new websites. While ThemeForest offers unprecedented choice and variety, there are huge pitfalls that all business owners should try and avoid when shopping for WordPress themes.

Let’s take a look at Avada, a premium multi-purpose Wordpress theme built by ThemeFusion and sold exclusively on ThemeForest, and its sales numbers. As of September 2016, this theme alone has sold 248,000 copies. Why is this theme so popular? It all draws down to the mini-templates available within Avada that allow it to be used for businesses in virtually any niche or industry. They have in-built templates for architects, churches, hotels, lawyers, doctors, and any other profession you can think of.

This versatility might seem like a positive, but it is ultimately the cause of the main issue with all of these popular ‘multi-purpose’ themes - bloatedness. Even though a dentist might only use the prebuilt dentist template on the Avada theme, that dentist’s website will also be holding all of the code for all of the other templates. The result is a website that feels sluggish and unresponsive. This is the reason that many of these themes use fancy loading screens in order to hide their excessive file sizes.

These themes are not very good from a search engine optimisation perspective either. Google rewards clean, semantic HTML5 markup and penalises sites that take too long to load. This is why you always better off going with either a custom designed theme or a smaller theme that is specifically designed for your industry.

This is why Brunel Web recommends StudioPress and its flagship Genesis Framework. All themes sold by StudioPress are built for a specific purpose (e.g. there are themes specifically built for health, fitness, fashion, etc.) which means they are much better optimised in terms of file size. Secondly, StudioPress is one of the few theme developers who correctly implement clean, semantic HTML5 markup - this is great from a SEO perspective.

We also recommend hiring professional web designers to create a custom WordPress theme for your business. A custom theme will always look much more professional than one bought on ThemeForest.
