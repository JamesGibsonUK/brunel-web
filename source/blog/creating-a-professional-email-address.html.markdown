---
title: Creating a professional email address for your business
date: 2016-09-02 08:51 UTC
description: A professional email address, using your own custom domain, is the best way to come across as trustworthy to potential customers.
---
There’s nothing more off-putting to potential customers than having to communicate with a salesperson who uses a free @gmail.com (or worse, @hotmail.com) email address. If your business is serious about projecting itself as trustworthy and professional, you without a doubt need to invest in a custom branded email address (e.g. john@yourcompany.com).

Fortunately, creating a professional email address can be done in less than 30 minutes for less than £5/month. Here are some of Brunel Web’s recommended providers:

## Google Apps
Google Apps is in a league of its own when it comes to hosting company email addresses. Alongside getting the stability that comes with being the world’s biggest internet company, Google Apps gives your employees seamless and integrated access to Google’s suite of web applications.

The real magic of Google Apps only starts to kick in when you decide to go ‘Full Google’ by adopting Google Docs in favour of Word, Google Sheets in favour of Excel, and Google Sheets in favour of Powerpoint. You’ll find these services are seamlessly integrated, accessible anywhere from any device, and completely free.  

In terms of email, you get full access to the standard Gmail client but instead now with your own domain name. This is a huge bonus as it’ likely your employees will already be familiar with the interface - so there’s no need to spend money on staff training.

## Exchange Online (Microsoft)
If you’re not ready to go ‘Full Google’, Exchange Online is Microsoft’s answer to custom-domain email hosting. This is a good option for companies already well integrated into the Office ecosystem - Exchange accounts can be added to Outlook in a matter of seconds.

This option actually works out slightly cheaper than Google Apps, but there are few downsides. Firstly, the online client isn’t as polished as Gmail’s. Secondly, you’ll have to pay extra if you want access to the full suite of Microsoft applications (i.e. an Office 365 subscription).


## Zoho Apps
If you are adamant on not paying for a professional email address (you may want to reconsider), Zoho Apps is your best bet. They will host custom domain email for free and also give access to their own suite of office applications.

Zoho’s online interface is vastly inferior to both Google’s and Microsofts - the setup process is also a bit trickier. For a free service, however, Zoho certainly does the job.

## Final Thoughts
Brunel Web recommend Google Apps to our web design clients because of its stellar user interface and general stability. If you’d like help creating a professional email address for you business, get in touch with Brunel Web.
